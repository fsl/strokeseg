import strokeseg
import sys

def main():
    ct = sys.argv[1]
    les = sys.argv[2]
    strokeseg.make_training_feature_file(ct, les)

if __name__ == "__main__":
    sys.exit(main())