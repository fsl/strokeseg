#!/usr/bin/env python
import os
import pandas as pd
import argparse
import sys
from glob import glob
import nibabel as nii
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("--excel", help="the master excel file describing the data", required=True)
parser.add_argument("--datadir", help="the root path of the data", required=True)
args = parser.parse_args()


def main():
    excelFile = args.excel
    datadir = os.path.abspath(args.datadir)
    if os.path.exists(os.path.abspath(excelFile)):
        df_full = pd.read_excel(excelFile)
        df_lesion = df_full[
            (df_full['Deliniated?'] == 1) &
            ((df_full['Type'] == "CT") | (df_full['Type'] == 'ct'))
        ]
        df_lesion.reset_index(inplace=True)
        df_lesion = df_lesion.filter(['ID', 'Type', 'LesionSide'])
        for i in range(df_lesion.shape[0]):
            df_lesion.loc[i, 'lesion_path'] = ''
            df_lesion.loc[i, 'ct_path'] = ''

            if "BL-" in df_lesion.loc[i, 'ID']:
                df_lesion.loc[i, 'ID'] = df_lesion.loc[i, 'ID'][3:]
            if os.path.exists(os.path.join(datadir, df_lesion.loc[i, 'ID'])):
                df_lesion.loc[i, 'exists'] = 1
                lesion_files = glob(
                    os.path.join(
                        datadir, df_lesion.loc[i, 'ID'], 'Lesions', '*esion.nii'
                    )
                )
                ct_files = glob(
                    os.path.join(
                        datadir, df_lesion.loc[i, 'ID'], 'Scans', '*CT.nii'
                    )
                )
                if (len(ct_files) > 0) & (len(lesion_files) > 0):
                    df_lesion.loc[i, 'lesion_path'] = ''
                    df_lesion.loc[i, 'ct_path'] = ''
                    break1 = False
                    break2 = False
                    for ct in ct_files:
                        ctnii = nii.load(ct)
                        for les in lesion_files:
                            lesnii = nii.load(les)
                            if ((np.array_equal(lesnii.header['dim'][1:4], ctnii.header['dim'][1:4])) &
                            (np.array_equal(lesnii.header['srow_x'], ctnii.header['srow_x'])) &
                            (np.array_equal(lesnii.header['srow_y'], ctnii.header['srow_y'])) &
                            (np.array_equal(lesnii.header['srow_z'], lesnii.header['srow_z']))):
                                df_lesion.loc[i, 'lesion_path'] = les
                                df_lesion.loc[i, 'ct_path'] = ct
                                break2 = True
                                break1 = True
                            if break2:
                                break
                        if break1:
                            break

            else:
                df_lesion.loc[i, 'exists'] = 0


    df_final = df_lesion
    indexNames = df_final[ df_final['lesion_path'] == '' ].index
    df_final.drop(indexNames , inplace=True)
    df_final.reset_index(drop=True, inplace=True)
    df_final.to_csv(os.path.abspath(excelFile) + '.csv')

if __name__ == "__main__":
    sys.exit(main())
