import os
import sys
import csv
import argparse
from shutil import copyfile


parser = argparse.ArgumentParser()
parser.add_argument("--csv", help="the filtered csv file describing the data", required=True)
parser.add_argument("--destdir", help="the folder to copy the data to", required=True)
args = parser.parse_args()

def main():
    csvfile = args.csv
    destdir = os.path.abspath(args.destdir)

    # read the csv file
    with open(csvfile) as f:
        reader = csv.reader(f)
        next(reader, None) # skip the header
        # loop through it and copy the participant files to the new destination
        for r in reader:
            print(r[0])
            les_src = r[4]
            ct_src = r[5]
            les = r[4].split('/')
            ct = r[5].split('/')
            pname_les = r[1].lower()
            pname_ct = r[1].lower()
            fname_les = les[-1]
            fname_ct = ct[-1]
            if pname_les == pname_ct:
                les_dest = os.path.join(
                        destdir,
                        "sub-"+pname_les,
                        "sub-"+pname_les+"_Lesion.nii")
                ct_dest = os.path.join(
                        destdir,
                        "sub-"+pname_ct,
                        "sub-"+pname_ct+"_CT.nii")
                #print(les_src)
                #print(les_dest)
                #print(ct_src)
                #print(ct_dest)
                os.makedirs(os.path.dirname(les_dest), exist_ok=True)
                copyfile(les_src, les_dest)
                copyfile(ct_src, ct_dest)
            #sys.exit()

if __name__ == '__main__':
    sys.exit(main())

