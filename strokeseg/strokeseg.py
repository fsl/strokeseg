#!/usr/bin/env fslpython

"""
the strokeseg module is designed to find a lesion within an image of a given modality.

modalities supported:
    - CT

future modalities:
    - MRI T1w
    - MRI T2w
    - MRI FLAIR

usage:

CLI: strokeseg --predict --modality ct --input img.nii.gz


"""
# TODO add logging and logging level specific debug messages
# builtin packages/modules
import os
import sys
import subprocess
from glob import glob
import pickle

# 3rd party packages/modules
import nibabel as nii
import numpy as np 
import pandas as pd
from scipy import ndimage
from scipy import spatial
from fsl.utils import path as fslpath
from fsl.data.image import ALLOWED_EXTENSIONS
from fsl.wrappers.flirt import flirt as fslflirt
from fsl.wrappers.flirt import applyxfm as fslapplyxfm
from fsl.wrappers.flirt import invxfm as fslinvxfm
from fsl.wrappers.fslmaths import fslmaths
from sklearn import cluster
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, plot_confusion_matrix
import matplotlib.pyplot as plt


FSLDIR = os.getenv("FSLDIR")
FSLDIR_BIN = os.path.join(FSLDIR, 'bin')
_fpth = os.path.abspath(__file__)
_ref_pth = os.path.join(
    os.path.dirname(_fpth),
    'scct_stripped.nii'
)
STROKESEG_CT_REF = _ref_pth



# class definitions
#------------------



# module wide functions
#----------------------
def h2c(h_name, c_name=None, remove_bone=False):
    """
    convert image in houndsfield units to cormack as in https://www.ncbi.nlm.nih.gov/pubmed/22440645

    h_name:          string file path of image in houndsfield units
    c_name:          string file path to save cormack image to
    remove_bone:    boolean, return c_str image with bone signal removed  

    returns:        c_name, the path of the saved image   

    
    """
    himg = nii.load(h_name)
    img_data = himg.get_fdata()
    I = np.copy(img_data)
    mn = np.min(I)
    mx = np.max(I)
    if mn < -1024:
        I[I < -1024] = -1024
        mn = np.min(I)
    rng = mx-mn
    SCALE = 10
    UNINTERESTING_DARK = 900
    INTERESTING_MID = 200
    I = I-mn
    extra1 = I - UNINTERESTING_DARK
    extra1[extra1 <= 0] = 0 # clip to zero
    extra9 = np.copy(extra1)
    extra9[extra9 > INTERESTING_MID] = INTERESTING_MID # clip to interesing max
    extra9 = extra9 * (SCALE-1)
    I = I + extra1 + extra9
    if remove_bone:
        I[I > 3000] = 100 # to remove bone if desired. But will also remove interesting blood related signal. and produce "zeros" holes in some areas
    cimg = nii.Nifti1Image(I, himg.affine, himg.header)
    cimg.header.set_slope_inter(1, 0)
    if c_name is None:
        c_name = fslpath.removeExt(h_name, allowedExts=ALLOWED_EXTENSIONS) + '_c.nii.gz'
    nii.save(cimg, c_name)
    return c_name

def ct_bet(in_pth, out_pth=None, f_val=0.4, erode=True, save_units=False):
    """
    bet an unprocessed CT brain image. h2c must not have been run on this image previously.
    """
    src_pth = in_pth
    c_in_pth = h2c(in_pth, remove_bone=True)

    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_brain.nii.gz'
    # initial bet to get mask
    cmd = [
        os.path.join(FSLDIR_BIN, 'bet'),
        c_in_pth,
        out_pth,
        '-f',
        str(f_val),
        '-m',
    ]
    subprocess.run(" ".join(cmd), shell=True, check=True)

    mask_pth = fslpath.removeExt(out_pth, allowedExts=ALLOWED_EXTENSIONS) + '_mask.nii.gz'
    if erode:
        cmd = [
            os.path.join(FSLDIR_BIN, 'fslmaths'),
            mask_pth,
            '-ero',
            mask_pth
        ]
        subprocess.run(" ".join(cmd), shell=True, check=True)
    # fslmaths used to multiply original image by brain mask. make minimum value 0
    if save_units:
        src_pth = in_pth
    else:
        src_pth = h2c(in_pth, remove_bone=False)
    cmd = [
        os.path.join(FSLDIR_BIN, 'fslmaths'),
        src_pth,
        '-mul',
        mask_pth,
        '-thr',
        '0',
        out_pth
    ]
    subprocess.run(" ".join(cmd), shell=True, check=True)
    os.remove(c_in_pth)
    return out_pth

def fsl_ct_reg(in_pth, les_pth=None, ref_pth=None, out_pth=None, dof=9, les_bin_thr=0.5):
    """
    use fsl flirt to register a brain extracted ct image to the rorden et al. 2012 ct template. 

    An alternative reference tempalte can be used if ref_pth is not None

    returns a tuple (image_path, mat_file_path)
    """
    les_out_pth = ''
    # default to using the rorden et al. 2012 ct template
    if ref_pth is None:
        ref_pth = STROKESEG_CT_REF

    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_ctnorm.nii.gz'
    out_mat = fslpath.removeExt(out_pth, allowedExts=ALLOWED_EXTENSIONS) + '.mat'
    cmd = [
        os.path.join(FSLDIR_BIN, 'flirt'),
        '-in',
        in_pth,
        '-ref',
        ref_pth,
        '-out',
        out_pth,
        '-omat',
        out_mat,
        '-dof',
        str(dof)
    ]
    subprocess.run(" ".join(cmd), shell=True, check=True)
    # apply the registration to the lesion mask
    if les_pth is not None:
        # make sure lesion mask is binary and 0..1
        cmd = [
            os.path.join(FSLDIR_BIN, 'fslmaths'),
            les_pth,
            '-bin',
            les_pth
        ]
        subprocess.run(" ".join(cmd), shell=True, check=True)

        les_out_pth = fslpath.removeExt(les_pth, allowedExts=ALLOWED_EXTENSIONS) + '_ctnorm.nii.gz'
        cmd = [
            os.path.join(FSLDIR_BIN, 'flirt'),
            '-in',
            les_pth,
            '-ref',
            out_pth,
            '-applyxfm',
            '-init',
            out_mat,
            '-interp',
            'nearestneighbour',
            '-out',
            les_out_pth
        ]
        subprocess.run(" ".join(cmd), shell=True, check=True)
        # now rebinarize lesion mask
        cmd = [
            os.path.join(FSLDIR_BIN, 'fslmaths'),
            les_out_pth,
            '-thr',
            str(les_bin_thr),
            '-bin',
            les_out_pth
        ]
        subprocess.run(" ".join(cmd), shell=True, check=True)
    return (out_pth, les_out_pth, out_mat)

def mirror_diff(in_pth, out_pth=None):
    """
    compute the difference between an input image and its mirror. If viewing in the axial plane
    the image is flipped L-->R style. 
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_mirrordiff.nii.gz'

    in_img = nii.load(in_pth)
    in_img_data = in_img.get_fdata()
    mirror = np.flipud(in_img_data)
    out_data = in_img_data - mirror
    out_img = nii.Nifti1Image(out_data, in_img.affine, in_img.header)
    nii.save(out_img, out_pth)
    return out_pth

def ref_diff(in_pth, ref_pth=None, out_pth=None):
    """
    compute the difference between an input image and a reference template image. 

    the reference image and the input image must be in the same space and at least linearly registered.
    """
    # default to using the rorden et al. 2012 ct template
    if ref_pth is None:
        fpth = os.path.abspath(__file__)
        ref_pth = os.path.join(
            os.path.dirname(fpth),
            'scct_stripped.nii'
        )

    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_refdiff.nii.gz'

    in_img = nii.load(in_pth)
    in_img_data = in_img.get_fdata()

    ref_img = nii.load(ref_pth)
    ref_img_data = ref_img.get_fdata()

    out_data = in_img_data - ref_img_data
    out_img = nii.Nifti1Image(out_data, in_img.affine, in_img.header)
    nii.save(out_img, out_pth)
    return out_pth


def fsl_median_blur(in_pth, out_pth=None, kernel_size="5 5 1"):
    """
    median blur using fslmaths from fsl. 
    
    For CT images it can be more appropriate
    to blur within thick axial slices rather than bluring over the anatomy across
    thick slices. Therefore the default kernel is set to 1 voxel for the z plane
    which is assumed to be axial
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_fmedian.nii.gz'
    cmd = [
        os.path.join(FSLDIR_BIN, 'fslmaths'),
        in_pth,
        '-kernel',
        'boxv3',
        kernel_size,
        '-fmedian',
        out_pth
    ]
    subprocess.run(" ".join(cmd), shell=True, check=True)
    return out_pth
    

def fsl_edges(in_pth, out_pth=None):
    """
    highlight edges in an image. Uses fsl's edge strengthen algorithm
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_edges.nii.gz'
    cmd = [
        os.path.join(FSLDIR_BIN, 'fslmaths'),
        in_pth,
        '-edge',
        out_pth
    ]
    subprocess.run(" ".join(cmd), shell=True, check=True)
    return out_pth

def fsl_ero_mask(in_pth, n=1, out_pth=None):
    """
    erode a binary mask using fslmaths
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_ero{}.nii.gz'.format(n)
    cmd = [
        os.path.join(FSLDIR_BIN, 'fslmaths'),
        in_pth,
        " ".join(['-ero'] * n),
        out_pth
    ]
    subprocess.run(" ".join(cmd), shell=True, check=True)
    return out_pth

def fsl_dil_mask(in_pth, n=1, out_pth=None):
    """
    dilate a binary mask using fslmaths
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_dil{}.nii.gz'.format(n)
    cmd = [
        os.path.join(FSLDIR_BIN, 'fslmaths'),
        in_pth,
        " ".join(['-dilF'] * n),
        out_pth
    ]
    subprocess.run(" ".join(cmd), shell=True, check=True)
    return out_pth

def distance_from_com(in_pth, out_pth=None, ref_pth=STROKESEG_CT_REF):
    """
    compute center of mass, and then get each voxel's distance from that coordinate. 
    Save the distance map as a new image. 

    if ref_pth is not None, then use the reference image to get the center of mass. 
    This will usually be a template image that the input image has been registered to.
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_dmap.nii.gz'
    in_img = nii.load(in_pth)
    in_data = in_img.get_fdata()
    dmap = np.copy(in_data) * 0
    if ref_pth is not None:
        ref_img = nii.load(ref_pth)
        ref_data = ref_img.get_fdata()
        c_x, c_y, c_z = ndimage.measurements.center_of_mass(ref_data)
    else:
        c_x, c_y, c_z = ndimage.measurements.center_of_mass(in_data)
    com = np.array([c_x, c_y, c_z]) # voxels, not mm
    aff = in_img.get_affine()
    com = nii.affines.apply_affine(aff, com) # now in mm

    for ind, _ in np.ndenumerate(in_data):
        coord = nii.affines.apply_affine(aff, np.array(ind)) # now convert to mm
        dmap[ind] = spatial.distance.euclidean(coord, com)

    out_img = nii.Nifti1Image(dmap, in_img.affine, in_img.header)
    nii.save(out_img, out_pth)
    return out_pth

def kmeans(in_pth, out_pth=None, k=5):
    """
    use kmeans clustering to do a rough segment of the input brain image.

    The input image must be brain extracted first
    """
    in_img = nii.load(in_pth)
    in_data = in_img.get_fdata()
    in_shape = in_data.shape
    I = in_data.reshape((-1, 1))

    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_kmeans.nii.gz'

    k_m = cluster.KMeans(n_clusters=k, random_state=42)
    _ = k_m.fit(I)
    clusters = k_m.labels_
    clusters = clusters.reshape(in_shape)
    out_img = nii.Nifti1Image(clusters, in_img.affine, in_img.header)
    nii.save(out_img, out_pth)
    return out_pth

def extract_lesion_signal(in_pth, les_pth):
    """
    extract only the voxels within a lesion mask. Useful for passing to kmeans clustering
    """
    in_img = nii.load(in_pth)
    in_data = in_img.get_fdata()
    in_shape = in_data.shape
    I = in_data.reshape(-1, 1)

    les_img = nii.load(les_pth)
    les_data = les_img.get_fdata()
    les_shape = les_data.shape
    L = les_data.reshape(-1, 1)

    les_ind = np.flatnonzero(L).reshape(-1, 1)
    les_subset = np.take(I, les_ind).reshape(-1, 1)
    return les_subset


def kmeans_les(in_pth, les_pth, out_pth=None, k=3):
    """
    use kmeans clustering to do a rough cluster of intensities within a lesion mask.

    The input image must be brain extracted first
    """
    in_img = nii.load(in_pth)
    in_data = in_img.get_fdata()
    in_shape = in_data.shape
    I = in_data.reshape(-1, 1)

    les_img = nii.load(les_pth)
    les_data = les_img.get_fdata()
    les_shape = les_data.shape
    L = les_data.reshape(-1, 1)

    les_ind = np.flatnonzero(L).reshape(-1, 1)
    les_subset = np.take(I, les_ind).reshape(-1, 1)

    if out_pth is None:
        out_pth = fslpath.removeExt(les_pth, allowedExts=ALLOWED_EXTENSIONS) + '_leskmeans.nii.gz'

    k_m = cluster.KMeans(n_clusters=k, random_state=42)
    _ = k_m.fit(les_subset)
    clusters = k_m.labels_ + 2 # plus 2 since lesion mask in binary (0 and 1), so start cluster indices at 2
    clusters.reshape(-1, 1)
    np.put(L, les_ind, clusters)
    OUT = L.reshape(in_shape)
    out_img = nii.Nifti1Image(OUT, les_img.affine, les_img.header)
    nii.save(out_img, out_pth)
    return out_pth

def kmeans_diff(in_pth, ref_pth=STROKESEG_CT_REF, out_pth=None, k=5):
    """
    compute the difference between two kmeans segmentations. In most cases, 
    the difference will be computed between an input image and the template it was registered to.

    The input image and the reference image must be at least linearly registered
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_kdiff.nii.gz'

    in_means = kmeans(in_pth, k=k)
    ref_means = kmeans(ref_pth, k=k)

    in_img = nii.load(in_means)
    ref_img = nii.load(ref_means)

    in_data = in_img.get_fdata()
    ref_data = ref_img.get_fdata()

    out_data = ref_data - in_data
    out_img = nii.Nifti1Image(out_data, in_img.affine, in_img.header)
    nii.save(out_img, out_pth)
    os.remove(ref_means)
    return out_pth


def make_training_feature_file(in_pth, les_pth, out_pth=None):
    """
    make features for a subject's input image. 

    save a csv text file with rows as voxels and colums as each feature per voxel

    All features will be in normalised space
    the voxel (rows) by feature (cols) matrix will be organised like this:

    lesion_label    brain   brain_blur  mirror_diff     ref_diff    kmeans  ref_kmeans      edges   com_dist  
    ...
    ...
    ...
    ...
    ...
    """

    columns=[]

    # set feature filename if not set
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_featurefile.pkl'

    bet_pth = ct_bet(in_pth)
    bet_blur_pth = fsl_median_blur(bet_pth, kernel_size='7 7 1')

    # feature 1 and 2 normalised lesion and brain CT image
    ct_reg_pth, les_reg_path, reg_mat_pth = fsl_ct_reg(bet_blur_pth, les_pth=les_pth)
    feat_ct = nii.load(ct_reg_pth).get_fdata().reshape(-1, 1)
    feat_les = nii.load(les_reg_path).get_fdata().reshape(-1, 1)
    features_mat = np.copy(feat_les)
    columns.append('lesion_label')

    # features_mat = np.hstack((features_mat, feat_les))
    features_mat = np.hstack((features_mat, feat_ct))
    columns.append('brain')
    
    # feature 3 normalised blurred brain image
    blur_pth = fsl_median_blur(ct_reg_pth, kernel_size='3 3 1')
    feat_brain_blur = nii.load(blur_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_brain_blur))
    columns.append('brain_blur')


    # feature 4 mirror difference image
    mirror_diff_pth = mirror_diff(ct_reg_pth)
    feat_mirror_diff = nii.load(mirror_diff_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_mirror_diff))
    columns.append('mirror_diff')

    # feature 5 reference difference image
    ref_diff_pth = ref_diff(ct_reg_pth)
    feat_ref_diff = nii.load(ref_diff_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_ref_diff))
    columns.append('ref_diff')


    # feature 6 ct brain kmeans
    kmeans_pth = kmeans(ct_reg_pth, k=5)
    feat_kmeans = nii.load(kmeans_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_kmeans))
    columns.append('kmeans')


    # feature 7 reference brain kmeans
    ref_kmeans_pth = fslpath.removeExt(ct_reg_pth, allowedExts=ALLOWED_EXTENSIONS) + '_refkmeans.nii.gz'
    ref_kmeans_pth = kmeans(STROKESEG_CT_REF, out_pth=ref_kmeans_pth, k=5)
    feat_ref_kmeans = nii.load(ref_kmeans_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_ref_kmeans))
    columns.append('ref_kmeans')


    # feature 8 edges
    edges_pth = fsl_edges(ct_reg_pth)
    feat_edges = nii.load(edges_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_edges))
    columns.append('edges')
    
    # feature 9 distance from com
    com_distmap_pth = distance_from_com(ct_reg_pth)
    feat_com_dist = nii.load(com_distmap_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_com_dist))
    columns.append('com_dist')


    # feature 10 kmeans within lesion
    les_kmeans_pth = kmeans_les(ct_reg_pth, les_reg_path, k=5)
    feat_les_kmeans = nii.load(les_kmeans_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_les_kmeans))
    columns.append('les_kmeans')

    # feature 11 eroded 1x lesion mask
    les_ero_pth = fsl_ero_mask(les_reg_path)
    feat_les_ero = nii.load(les_ero_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_les_ero))
    columns.append('les_ero')

    # feature 12 eroded 2x lesion mask
    les_ero2_pth = fsl_ero_mask(les_reg_path, n=2)
    feat_les_ero2 = nii.load(les_ero2_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_les_ero2))
    columns.append('les_ero2')

    # feature 13 eroded 3x lesion mask
    les_ero3_pth = fsl_ero_mask(les_reg_path, n=3)
    feat_les_ero3 = nii.load(les_ero3_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_les_ero3))
    columns.append('les_ero3')

    # feature 14 dilated lesion mask
    les_dil_pth = fsl_dil_mask(les_reg_path)
    feat_les_dil = nii.load(les_dil_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_les_dil))
    columns.append('les_dil1')

    # construct dataframe from numpy array
    df = pd.DataFrame(
        data=features_mat,
        columns=columns    
        )
    # save dataframe to pickle file
    df.to_pickle(out_pth)
    return out_pth


def make_prediction_feature_file(in_pth, out_pth=None):
    """
    make features for a subject's input image. 

    """

    data_dict = {}
    columns=[]

    # set feature filename if not set
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_featurefile.pkl'

    bet_pth = ct_bet(in_pth)
    data_dict['original_image'] = in_pth
    bet_blur_pth = fsl_median_blur(bet_pth, kernel_size='7 7 1')

    # feature 1 and 2 normalised lesion and brain CT image
    ct_reg_pth, _, reg_mat_pth = fsl_ct_reg(bet_blur_pth)
    feat_ct = nii.load(ct_reg_pth).get_fdata().reshape(-1, 1)
    features_mat = np.copy(feat_ct)
    columns.append('brain')
    data_dict['normalised_image'] = ct_reg_pth
    data_dict['orig_to_std_mat'] = reg_mat_pth
    
    # feature 3 normalised blurred brain image
    blur_pth = fsl_median_blur(ct_reg_pth, kernel_size='3 3 1')
    feat_brain_blur = nii.load(blur_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_brain_blur))
    columns.append('brain_blur')


    # feature 4 mirror difference image
    mirror_diff_pth = mirror_diff(ct_reg_pth)
    feat_mirror_diff = nii.load(mirror_diff_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_mirror_diff))
    columns.append('mirror_diff')

    # feature 5 reference difference image
    ref_diff_pth = ref_diff(ct_reg_pth)
    feat_ref_diff = nii.load(ref_diff_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_ref_diff))
    columns.append('ref_diff')


    # feature 6 ct brain kmeans
    kmeans_pth = kmeans(ct_reg_pth, k=5)
    feat_kmeans = nii.load(kmeans_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_kmeans))
    columns.append('kmeans')


    # feature 7 reference brain kmeans
    ref_kmeans_pth = fslpath.removeExt(ct_reg_pth, allowedExts=ALLOWED_EXTENSIONS) + '_refkmeans.nii.gz'
    ref_kmeans_pth = kmeans(STROKESEG_CT_REF, out_pth=ref_kmeans_pth, k=5)
    feat_ref_kmeans = nii.load(ref_kmeans_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_ref_kmeans))
    columns.append('ref_kmeans')


    # feature 8 edges
    edges_pth = fsl_edges(ct_reg_pth)
    feat_edges = nii.load(edges_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_edges))
    columns.append('edges')
    
    # feature 9 distance from com
    com_distmap_pth = distance_from_com(ct_reg_pth)
    feat_com_dist = nii.load(com_distmap_pth).get_fdata().reshape(-1, 1)

    features_mat = np.hstack((features_mat, feat_com_dist))
    columns.append('com_dist')

    # construct dataframe from numpy array
    df = pd.DataFrame(
        data=features_mat,
        columns=columns    
        )
    # save dataframe to pickle file
    df.to_pickle(out_pth)
    return out_pth, data_dict, df

def save_lesion_intensity(in_pth, les_pth, out_pth=None, print_val=False, overwrite=False, use_median=False, use_max=False):
    """
    take a ct image and a lesion mask and report the average intensity
    within the mask. 

    a text file will be saved containing the average value

    returns a dict of:
    {
        'ct': str,
        'les': str,
        'signal': float
    }
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(les_pth, allowedExts=ALLOWED_EXTENSIONS) + '_avg.txt'

    if os.path.isfile(out_pth):
        signal = load_lesion_intensity(out_pth)
        outdict = {
        'ct': in_pth,
        'lesion': les_pth,
        'signal': signal
        }
        if not overwrite:
            return outdict


    ct = nii.load(in_pth).get_fdata().reshape(-1, 1)
    les = nii.load(les_pth).get_fdata().reshape(-1, 1)

    les_ind = np.flatnonzero(les).reshape(-1, 1)
    les_subset = np.take(ct, les_ind).reshape(-1, 1)

    if use_median:
        signal = np.median(les_subset)
    else:
        signal = np.mean(les_subset)

    if use_max:
        signal = np.max(les_subset)
    else:
        signal = np.mean(les_subset)

    if print_val:
        print("within-lesion intensity: {}".format(signal))
    np.savetxt(out_pth, np.array([signal]))
    outdict = {
        'ct': in_pth,
        'lesion': les_pth,
        'signal': signal
    }
    return outdict

def _get_image_pairs(base_dir):
    """
    return ct and lesion image pairs from a base_dir with the format:

    base_dir
        sub-001
            sub-001_CT.nii
            sub-001_Lesion.nii
        sub-002
            sub-002_CT.nii
            sub-002_Lesion.nii
    """
    subj_dirs = glob(os.path.join(base_dir, 'sub-*'))
    image_pairs = []

    for subj in subj_dirs:
        lesion = glob(os.path.join(subj, '*Lesion.nii'))
        ct = glob(os.path.join(subj, '*CT.nii'))
        image_pairs.append(
            {
                'ct': ct[0],
                'lesion': lesion[0]
            }
        )
    return image_pairs

def scatterplot(vals, labels=None, colors=None):
    """
    scatter plot of anything
    """
    x = np.arange(0, len(vals))
    plt.scatter(x, vals, c=colors, alpha=0.8)
    if labels is not None:
        for i, label in enumerate(labels):
            plt.text(x[i], vals[i], label, fontsize=8)
    plt.show()

def _plot_all_lesion_intensity(base_dir):
    """
    plot the average lesion intensity for each subject in base_dir
    """
    a = np.array([])
    labels = []
    pairs = _get_image_pairs(base_dir)
    for pair in pairs:
        v = save_lesion_intensity(pair['ct'], pair['lesion'])
        a = np.append(a, v['signal'])
        labels.append(v['ct'])
    
    scatterplot(a, labels)

def _split_subjs_kmeans(base_dir, k=2, overwrite=False, use_max=False, plot=False):
    """
    split subjects based on lesion intensity kmeans clustering for each subject in base_dir
    """
    low_csv = os.path.join(base_dir, 'df_low.csv')
    high_csv = os.path.join(base_dir, 'df_high.csv')
    # in hounsfield units
    low_lim = 30
    high_lim = 45
    a = np.array([])
    labels = []
    pairs = _get_image_pairs(base_dir)
    df_col_names = ['ct', 'lesion', 'signal']
    # dataframe for low signal intensity (ischemic)
    df_low = pd.DataFrame(columns=df_col_names)
    # dataframe for high signal intensity (bleed)
    df_high = pd.DataFrame(columns=df_col_names)
    for pair in pairs:
        v = save_lesion_intensity(pair['ct'], pair['lesion'], overwrite=overwrite, use_max=use_max)
        a = np.append(a, v['signal']) 
        labels.append(v['ct'])
        # make pandas dataframe for results
        if v['signal'] < low_lim:
            df_tmp = pd.DataFrame([[v['ct'], v['lesion'], v['signal']]], columns=df_col_names)
            df_low = df_low.append(df_tmp, ignore_index=True)
        elif v['signal'] > high_lim:
            df_tmp = pd.DataFrame([[v['ct'], v['lesion'], v['signal']]], columns=df_col_names)
            df_high = df_high.append(df_tmp, ignore_index=True)
    df_low.to_csv(low_csv, index=False)
    df_high.to_csv(high_csv, index=False)

    if plot:
        k_m = cluster.KMeans(n_clusters=k, random_state=42)
        a = np.reshape(a, (-1, 1))
        _ = k_m.fit(a)
        clusters = k_m.labels_
        scatterplot(a, labels=labels, colors=clusters)


def _lesion_inertia_test(data_csv, min_k=2, max_k=10, plot=True, n=50):
    """
    read in a data csv file and plot kmeans inertia test results

    data_csv must contain header column names: 'ct', 'lesion'
    """
    df = pd.read_csv(data_csv) 
    lesion_data = np.array([])
    inertia_vals = []
    for index, row in df.iterrows():
        if index > n:
            break
        print('dataframe index: ', index)
        lesion_data = np.append(
            lesion_data,
            extract_lesion_signal(row['ct'], row['lesion'])
        )

    for k in range(min_k, max_k+1):
        print('k index: ', k)
        k_m = cluster.KMeans(n_clusters=k, random_state=42)
        lesion_data = np.reshape(lesion_data, (-1, 1))
        _ = k_m.fit(lesion_data)
        inertia = k_m.inertia_
        inertia_vals.append(inertia)

    if plot:
        plt.plot(range(min_k, max_k+1), inertia_vals, 'x-')
        plt.xlabel('k')
        plt.ylabel('inertia')
        plt.title('Finding Optimal k')
        plt.show()


def split_subjects_train_test(data_csv, test_size=0.2, random_state=42):
    """
    split dataset subjects into train and test dataframes. the subject dataset is loaded
    from a csv file. 

    returns two dataframes, train and test

    these dataframes are also saved to the same location as data_csv
    """
    csv_noext = os.path.splitext(data_csv)[0]
    train_file = csv_noext + '_train.pkl'
    test_file = csv_noext + '_test.pkl'
    df = pd.read_csv(data_csv)
    train_subjs_df, test_subjs_df = train_test_split(df, test_size=test_size, random_state=random_state)
    train_subjs_df.to_csv(csv_noext + '_train.csv')
    test_subjs_df.to_csv(csv_noext + '_test.csv')
    idx = 0
    for _, row in train_subjs_df.iterrows():
        ct_noext = os.path.splitext(row['ct'])[0]
        featurefile = ct_noext + '_featurefile.pkl'
        if not os.path.isfile(featurefile):
            idx = idx + 1
            continue
        print('train dataframe index: {} of {}'.format(idx, len(train_subjs_df)-1))
        print(row['ct'])
        if idx == 0:
            train_df = sample_subject_features(featurefile)
        else:
            train_df = train_df.append(sample_subject_features(featurefile))
        idx = idx + 1

    idx = 0
    for _, row in test_subjs_df.iterrows():
        ct_noext = os.path.splitext(row['ct'])[0]
        featurefile = ct_noext + '_featurefile.pkl'
        if not os.path.isfile(featurefile):
            idx = idx + 1
            continue
        print('test dataframe index: {} of {}'.format(idx, len(test_subjs_df)-1))
        print(row['ct'])
        if idx == 0:
            test_df = sample_subject_features(featurefile)
        else:
            test_df = test_df.append(sample_subject_features(featurefile))
        idx = idx + 1

    train_df.to_pickle(train_file)
    test_df.to_pickle(test_file)

    return train_df, test_df


def sample_subject_features(feature_file, n_samples_lesion=200, n_samples_nonlesion=200, random_state=42):
    """
    load a featurefile (a dataframe pickle) and randomly sample equal numbers of lesioned
    and unlesioned voxels

    return the subsampled dataframe
    """
    full_df = pd.read_pickle(feature_file)
    les_df = full_df[full_df.lesion_label == 1]
    if len(les_df) < n_samples_lesion:
        print('Number of lesion voxels is less than n_samples_lesion in file: {}'.format(feature_file))
        print('only using the max available lesioned voxels: n={}'.format(len(les_df)))
        n_samples_lesion = len(les_df)
        n_samples_nonlesion = n_samples_lesion

    non_les_df = full_df[full_df.lesion_label == 0]

    les_perm = np.random.permutation(len(les_df))
    nonles_perm = np.random.permutation(len(non_les_df))

    les_idx = les_perm[:n_samples_lesion]
    non_les_idx = nonles_perm[:n_samples_nonlesion]

    sample_df = les_df.iloc[les_idx]
    sample_df = sample_df.append(non_les_df.iloc[non_les_idx])
    return sample_df   


def load_lesion_intensity(in_pth):
    """
    load a text file from storage. An intensity will be a single value
    """
    intensity = np.loadtxt(in_pth)
    return intensity

def train_randomforest(train_data_file, test_data_file):
    """
    train a random forest model on the training data
    """
    full_train_df = pd.read_pickle(train_data_file)
    train_data = full_train_df.drop(['lesion_label', 'les_dil1', 'les_ero3', 'les_ero2', 'les_ero', 'les_kmeans'], axis=1)
    # train_labels = full_train_df['lesion_label'].copy()
    train_labels = full_train_df['les_kmeans'].copy()

    full_test_df = pd.read_pickle(test_data_file)
    test_data = full_test_df.drop(['lesion_label', 'les_dil1', 'les_ero3', 'les_ero2', 'les_ero', 'les_kmeans'], axis=1)
    # test_labels = full_test_df['lesion_label'].copy()
    test_labels = full_test_df['les_kmeans'].copy()
    feat_names = train_data.columns
    print(feat_names)


    pipeline = Pipeline(
        [
            ('min_max_scaler', MinMaxScaler())
        ]
    )

    train_data_transformed = pipeline.fit_transform(train_data)
    test_data_transformed = pipeline.transform(test_data)

    randomforest_model = RandomForestClassifier(n_estimators=100, n_jobs=-1, random_state=42)
    randomforest_model.fit(train_data_transformed, train_labels)

    predicted_labels = randomforest_model.predict(test_data_transformed)
    print(confusion_matrix(test_labels, predicted_labels))
    print(classification_report(test_labels, predicted_labels))
    print(accuracy_score(test_labels, predicted_labels))
    pipeline_file = os.path.join(
        os.path.dirname(__file__), 'randomforest_pipeline.pkl'
    )
    model_file = os.path.join(
        os.path.dirname(__file__), 'randomforest_model.pkl'
    )
    pickle.dump(pipeline, open(pipeline_file, 'wb'))
    pickle.dump(randomforest_model, open(model_file, 'wb'))

    importances = randomforest_model.feature_importances_
    std = np.std([tree.feature_importances_ for tree in randomforest_model.estimators_],
             axis=0)
    indices = np.argsort(importances)[::-1]

    # Print the feature ranking
    print("Feature ranking:")

    for f in range(len(importances)):
        print("{}. {}".format(feat_names[indices[f]], importances[indices[f]]))

    # plot_confusion_matrix(randomforest_model, train_data_transformed, test_data_transformed)


def train_voting_classifier(train_data_file, test_data_file):
    """
    train a voting model on the training data
    """
    full_train_df = pd.read_pickle(train_data_file)
    train_data = full_train_df.drop(['lesion_label', 'les_dil1', 'les_ero3', 'les_ero2', 'les_ero', 'les_kmeans'], axis=1)
    train_labels = full_train_df['lesion_label'].copy()

    full_test_df = pd.read_pickle(test_data_file)
    test_data = full_test_df.drop(['lesion_label', 'les_dil1', 'les_ero3', 'les_ero2', 'les_ero', 'les_kmeans'], axis=1)
    test_labels = full_test_df['lesion_label'].copy()

    pipeline = Pipeline(
        [
            ('min_max_scaler', MinMaxScaler())
        ]
    )

    train_data_transformed = pipeline.fit_transform(train_data)
    test_data_transformed = pipeline.transform(test_data)

    rf_model = RandomForestClassifier(n_estimators=100, n_jobs=-1, random_state=42)
    log_model = LogisticRegression()
    svm_model = SVC()

    voting_model = VotingClassifier(
        estimators=[
            ('logistic', log_model),
            ('forest', rf_model),
            ('svm', svm_model)
        ],
        voting='hard'
    )
    voting_model.fit(train_data_transformed, train_labels)

    for model in (rf_model, log_model, svm_model, voting_model):
        model.fit(train_data_transformed, train_labels)
        predicted_labels = model.predict(test_data_transformed)
        print(model.__class__.__name__, accuracy_score(test_labels, predicted_labels))


def predict_randomforest(in_pth, out_pth=None):
    """
    predict untrained data using the random forest model

    in_pth is the full path to a CT image
    """
    if out_pth is None:
        out_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_strokeseg.nii.gz'
        predicted_std_pth = fslpath.removeExt(in_pth, allowedExts=ALLOWED_EXTENSIONS) + '_predicted_ctnorm.nii.gz'

    pipeline_file = os.path.join(
        os.path.dirname(__file__), 'randomforest_pipeline.pkl'
    )
    model_file = os.path.join(
        os.path.dirname(__file__), 'randomforest_model.pkl'
    )
    
    pipeline = pickle.load(open(pipeline_file, 'rb'))
    model = pickle.load(open(model_file, 'rb'))
    # orig_dim = nii.load(in_pth).header['dim'][1:4]
    # print(orig_dim)
    _, data_dict, test_data = make_prediction_feature_file(in_pth)
    orig_dim = nii.load(data_dict['original_image']).header['dim'][1:4]
    std_img = nii.load(data_dict['normalised_image'])
    std_dim = std_img.header['dim'][1:4]
    reg_mat = data_dict['orig_to_std_mat']
    inv_mat = os.path.splitext(reg_mat)[0] + '_inv' + os.path.splitext(reg_mat)[-1]
    fslinvxfm(reg_mat, inv_mat)

    test_data_transformed = pipeline.transform(test_data)
    predicted_labels = model.predict(test_data_transformed)
    predicted_labels_proba = model.predict_proba(test_data_transformed)

    predicted_labels_proba = predicted_labels_proba[:, 1]
    predicted_lesion = np.reshape(predicted_labels_proba, tuple(std_dim))
    # predicted_lesion_cp = np.copy(predicted_lesion)
    # predicted_lesion_cp[predicted_lesion_cp < 0.5] = 0
    # lesion_bin = np.copy(predicted_lesion_cp)
    # lesion_bin[lesion_bin > 0] = 1
    # lesion_bin = skbinary_opening(lesion_bin)
    # lesion_objects = sklabel(lesion_bin, connectivity=2)
    # lesion_filtered = skremove_small_objects(lesion_objects, min_size=128)

    # predicted_lesion = predicted_lesion * lesion_filtered

    out_img = nii.Nifti1Image(predicted_lesion, std_img.affine, std_img.header)
    nii.save(out_img, predicted_std_pth)

    fslapplyxfm(predicted_std_pth, in_pth, inv_mat, out_pth)
    fslmaths(out_pth).uthr(1.0).thr(0.0).run(output=out_pth)
    # TODO remove small elements from final output in native space 
    # structuring element should be something like np.ones((10,10,1))
    # things smaller than the element will be removed 
    # also probably do an fslmaths binv
    # https://docs.scipy.org/doc/scipy-0.16.1/reference/generated/scipy.ndimage.morphology.binary_fill_holes.html

    

    

def predict():
    pass

# main program
#-------------
def _main():
    """
    the main program that runs from the command line call

    if you want to use this module in other python code then 
    you can import strokeseg and call strokeseg.predict on an image 
    to get a lesion segmentation
    """
    pass


if __name__ == "__main__":
    sys.exit(_main())




