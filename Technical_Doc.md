---
title: "Automated Ischemic Stroke Segmentation in CT Images with the Option of Manual Intervention"
author: 
- "Taylor Hanayik"
- "Nele Demeyere"
- "Margaret Moore"
- "Mark Jenkinson"

---

# strokeseg

`strokeseg` is a python package and command line program to identify stroke in clinical brain images. 

# Supported modalities

CT
 
# Usage
 
`strokeseg` can be used as a command line program or in other python scripts via an API
 
## command line
 
 ```
 strokeseg --in <input_image> --modality <ct> --out [default: input_image + _sslesion]
 ```

# Input Data Description

The input data consist of NIFTI files from more than one modality. Participant folders are organised by and ID. Participant folders contain multiple NIFTI image files within subfolders labeled *Scans*, *Lesions*, and  *StandardSpace*. Within the *Scans* subfolder there can be multiple NIFTI image files from more than one modality, or multiple image files of the same modality. For example, there can be multiple CT images present. Within the *Lesions* subfolder there can be multiple image files in either the *.nii* or *.voi* formats. These lesions have been traced by hand. The *StandardSpace* subfolder can contain images that have been registered to the Rorden clinical CT template. 

There is also a *Master_DeliniationLog.xlsx* that contains additional information about each participant folder. There are more participants listed in the log than participant folders in the imaging dataset. 

# Data Preparation

## Filtering the participant files to use

The *Master_DeliniationLog* is a source file of sorts, and the entry point to organising the imaging data. The log file is read into a `pandas` dataframe and parsed. A subset dataframe is created from the full set. Only entries where **Type** is CT in either lower case or upper case, and **Deliniated?** is True are kept. This subset dataframe is then further filtered to only include participants that undoubtedly have one lesion and one CT image in the same space. These filtering steps compare the image header information such as the size of each dimension, and the affine matrix. 

The final, filtered dataframe contains the fields: **ID**, **Type**, **LesionSide**, **exists**, **lesion_path**, **ct_path**.

The final, filtered dataframe is then stored in CSV format.

The filtered dataframe contains **430** sets of participant images for inclusion in the project data.

Use the python script *copy_filtered_data.py* in order to make a copy of this dataset that we will use for extracting our voxelwise features. This is saved in a BIDs formatted structure. 

## Image Preprocessing

### Modify the tissue type ranges

Our raw CT images are saved in Houndsfield units. We need to modify the intensity ranges for tissues in order to match the MNI space template used for Normalisation [ref](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3376197/pdf/nihms364329.pdf)

Use the *h2c.py* script to accomplish this. Copied from [clinical_h2c.m](https://github.com/neurolabusc/Clinical/blob/master/clinical_h2c.m)

### Noise removal of raw reconstructed CT images

Most CT images in the dataset have major noise artefact. This noise appears as a "salt and pepper" effect in the images. A noise removal process must be used to prepare the data for input to lesion segmentation machine learning models. 

Noise removal options:
- median filter [3, 5, 7, 9 in-plane voxel kernels if possible]
- https://iopscience.iop.org/article/10.1088/2057-1976/2/4/045015/pdf
- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3491154/
- https://arxiv.org/pdf/1807.03119.pdf

 
