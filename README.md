---
title: "Automated Ischemic Stroke Segmentation in CT Images with the Option of Manual Intervention"
author: 
- "Taylor Hanayik"
- "Nele Demeyere"
- "Margaret Moore"
- "Mark Jenkinson"

---

# THIS SOFTWARE IS A WORK IN PROGRESS. SOME FEATURES MAY BE MISSING, AND CODE MAY CHANGE AT ANY TIME.

# strokeseg

strokeseg is a python package + command line program to identify stroke in clinical brain images. 

## Google Docs paper

https://docs.google.com/document/d/1OQ-MDA2rz2U7_hrYfsnHvyYL9giQ0xPf6L7uKQIVtGk/edit?usp=sharing

## Supported modalities

 - CT
 
 ## Developers

To start developing for strokeseg, make a new virtual environment

```

# Change directories to where you would like to save the environment

cd /path/you/want

# create a new venv

python3 -m venv strokeseg
 
# activate the new environment

source /path/you/want/strokeseg/bin/activate

# change directory into the repo

cd /path/to/strokeseg_repo

# install the project requirements into the virtual environment (assuming it is active)

pip3 install -r requirements.txt

```

## Data analysis 

### Notes

subjects to exclude:

- p0226
    - lesion mask includes many whole slices in z-axis
- p5140
    - lesion mask includes some whole slices in z axis
